namespace UTM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class employeeJob : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "JobID", c => c.Int());
            CreateIndex("dbo.Employees", "JobID");
            AddForeignKey("dbo.Employees", "JobID", "dbo.Jobs", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "JobID", "dbo.Jobs");
            DropIndex("dbo.Employees", new[] { "JobID" });
            DropColumn("dbo.Employees", "JobID");
        }
    }
}
