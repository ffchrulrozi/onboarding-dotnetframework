namespace UTM.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_column_weight_table_hewan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Hewans", "Weight", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Hewans", "Weight");
        }
    }
}
