﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UTM.Startup))]
namespace UTM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
