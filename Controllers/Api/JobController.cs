﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UTM.Models;
using UTM.DAL;

namespace UTM.Controllers.Api
{
    public class JobController : ApiController
    {
        private UTMContext db = new UTMContext();

        public HttpResponseMessage GetJobs()
        {
            return Request.CreateResponse(db.Jobs.ToList());
        }
    }
}
