﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using UTM.Models;
using UTM.DAL;

namespace UTM.Controllers.Api
{
    public class EmployeeController : ApiController
    {
        private UTMContext db = new UTMContext();

        // GET: api/Employee
        // public IQueryable<Employee> GetEmployees()
        public HttpResponseMessage GetEmployees(int draw=1, int length=10, int start=0, string search="")
        {
            var response = new List<Dictionary<string, object>>();

            var employees = db.Employees.OrderBy(x => x.Name).Skip(start).Take(length).ToList();

            foreach (var employee in employees)
            {
                response.Add(new Dictionary<string, object>(){
                    { "id",employee.ID },
                    { "name",employee.Name },
                    { "sex", employee.Sex.ToString() },
                    { "birthdate", employee.Birthdate.ToString() },
                    { "religion", employee.Religion.ToString() },
                    { "job", employee.Job.Name.ToString() }
                });
            }

            return Request.CreateResponse(new Dictionary<string, object>()
            {
                { "draw", draw },
                { "recordsTotal", db.Employees.Count() },
                { "recordsFiltered", db.Employees.Count() },
                { "data", response }
            });
        }

        // GET: api/Employee/5
        [ResponseType(typeof(Employee))]
        public IHttpActionResult GetEmployee(int id)
        {
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return NotFound();
            }

            return Ok(employee);
        }

        // PUT: api/Employee/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEmployee(int id, Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != employee.ID)
            {
                return BadRequest();
            }

            db.Entry(employee).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Employee
        [ResponseType(typeof(Employee))]
        public IHttpActionResult PostEmployee(Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Employees.Add(employee);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = employee.ID }, employee);
        }

        // DELETE: api/Employee/5
        [ResponseType(typeof(Employee))]
        public IHttpActionResult DeleteEmployee(int id)
        {
            Employee employee = db.Employees.Find(id);
            if (employee == null)
            {
                return NotFound();
            }

            db.Employees.Remove(employee);
            db.SaveChanges();

            return Ok(employee);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EmployeeExists(int id)
        {
            return db.Employees.Count(e => e.ID == id) > 0;
        }
    }
}