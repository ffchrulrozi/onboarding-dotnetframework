﻿using UTM.Models;
using System.Data.Entity;
namespace UTM.DAL
{
    public class UTMContext : DbContext
    {
        public UTMContext() : base("UTMDBase") { }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Job> Jobs{ get; set; }
        public DbSet<Hewan> Hewans{ get; set; }

    }
}