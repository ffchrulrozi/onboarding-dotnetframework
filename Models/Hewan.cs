﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UTM.Models
{
    public class Hewan
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Weight { get; set; }
    }
}