﻿using System;
using UTM.Models;

namespace UTM.Models
{
    public enum Religion
    {
        Islam, Khatolik, Protestan, Hindu, Buddha, Konghuchu, Lainnya
    }

    public enum Sex
    {
        Pria, Wanita
    }
    public class Employee
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Sex Sex { get; set; }
        public DateTime Birthdate { get; set; }
        public Religion Religion { get; set; }
        public string Photo { get; set; }
        public int? JobID { get; set; }
        public virtual Job Job { get; set; }
    }
}